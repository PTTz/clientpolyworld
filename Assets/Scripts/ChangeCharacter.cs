using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCharacter : MonoBehaviour
{
    [SerializeField]
    public GameObject[] CharacterList;
    public int PickedNumber = 1;

    public void ChangePickedCharacter(int PickedCharacter)
    {
        PickedNumber = PickedCharacter;
        var length = CharacterList.Length;
        for (int i = 0; i < length; i++)
        {
            if (i == PickedCharacter)
            {
                CharacterList[i].active = true;
            }
            else
            {
                CharacterList[i].active = false;
            }
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
