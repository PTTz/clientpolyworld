using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerMove : MonoBehaviour
{
    public GameObject player;
    public Rigidbody rb_player;
    public float speed;
    PhotonView view;
    // Start is called before the first frame update
    void Start()
    {
        view = GetComponent<PhotonView>();
    }

    // Update is called once per frame
    void Update()
    {
        if (view.IsMine)
        {
            if ((Input.GetKey(KeyCode.A)))
            {
                rb_player.velocity = new Vector3(-speed, 0, 0);
            }
            if ((Input.GetKey(KeyCode.D)))
            {
                rb_player.velocity = new Vector3(speed, 0, 0);
            }
            if ((Input.GetKey(KeyCode.W)))
            {
                rb_player.velocity = new Vector3(0, speed, 0);
            }
            if ((Input.GetKey(KeyCode.S)))
            {
                rb_player.velocity = new Vector3(0, -speed, 0);
            }
        }

    }
}
