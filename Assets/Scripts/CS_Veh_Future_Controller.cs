
using UnityEngine;
using PathCreation;

public class CS_Veh_Future_Controller : MonoBehaviour
{
    [Header("------------- Vehicle Stats ---------------")]
    [SerializeField] [Range(100, 1000)]
    private int Condition;
    [SerializeField] [Range(100,500)]
    private int Ammor;
    [SerializeField] [Range(100, 1000)]
    private int HorsePower;

    [Header("------------- Vehicle Engine --------------")]
    [SerializeField]
    private bool AutoPilot;
    [SerializeField]
    private bool EngineOn;
    [SerializeField]

    [Header("------------- Vehicle FX ------------------")]
    public GameObject[] ExhaustTrails;
    public GameObject[] BoosterFlames;
}
