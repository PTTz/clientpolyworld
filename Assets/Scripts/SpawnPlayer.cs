using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class SpawnPlayer : MonoBehaviour
{
    public GameObject player_prefab;
    public GameObject spawnPoint;
    public GameObject CnJ;
    public int PickedCharacter = 1;
    // Start is called before the first frame update
    void Start()
    {
        CnJ = GameObject.FindGameObjectWithTag("EventGameObj");
        PickedCharacter = CnJ.GetComponent<CreateAndJoin>().PickedCharacter;
        DestroyObject(CnJ);
        CreatePlayer();
    }

    public void CreatePlayer()
    {
/*        PhotonNetwork.Instantiate(player_prefab.name, spawnPoint.transform.position, Quaternion.Euler(0, 0, 0));*/
        var newPlayer = PhotonNetwork.Instantiate(player_prefab.name, spawnPoint.transform.position, Quaternion.Euler(0, 0, 0));
        newPlayer.GetComponent<ChangeCharacter>().ChangePickedCharacter(PickedCharacter);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
