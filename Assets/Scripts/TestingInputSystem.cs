using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class TestingInputSystem : MonoBehaviour
{
    private PlayerInput playerInput;

    private void Awake()
    {
        playerInput = GetComponent<PlayerInput>();
        
    }



    public void Jump(InputAction.CallbackContext context)
    {
        Debug.Log(context);
        if(context.performed)
            Debug.Log("JUMP" + context.phase);
    }
}
