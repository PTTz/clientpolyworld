using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class CreateAndJoin : MonoBehaviourPunCallbacks
{
    // Start is called before the first frame update
    [SerializeField]
    public savePickedCharacter save;
    [SerializeField]
    public GameObject[] CharacterList;
    public int PickedCharacter = 0;
    public void OnClickChangeToLeft()
    {
        if (PickedCharacter == 0)
        {
            PickedCharacter = CharacterList.Length-1;
        }
        else
        {
            PickedCharacter -= 1;
        }
        ChangePickedCharacter();
    }
    public void OnClickChangeToRight()
    {
        if (PickedCharacter == CharacterList.Length-1)
        {
            PickedCharacter = 0;
        }
        else
        {
            PickedCharacter += 1;
        }
        ChangePickedCharacter();
    }
    public void ChangePickedCharacter()
    {
        var length = CharacterList.Length;
        for (int i = 0; i < length; i++)
        {
            if (i == PickedCharacter)
            {
                CharacterList[i].active = true;
            }
            else
            {
                CharacterList[i].active = false;
            }
        }
    }
    public void JoinOrCreate()
    {
        PhotonNetwork.JoinOrCreateRoom("room", null,null);
    }
    private void OnPhotonJoinRoomFailed(object[] codeAndMsg)
    {
        Debug.LogErrorFormat("Room join failed with error code {0} and error message {1}", codeAndMsg[0], codeAndMsg[1]);
    }
    public override void OnJoinedRoom()
    {
        PhotonNetwork.LoadLevel("DemoScifiCity");
    }
    // Update is called once per frame
    public void Start()
    {
        DontDestroyOnLoad(gameObject);
    }
    void Update()
    {
        
    }
}
